<?php

/**
 * @file
 * Contains the cision_block.module.
 */

define('CISION_BLOCK_DEFAULT_COUNT', 50);
define('CISION_BLOCK_MIN_COUNT', 1);
define('CISION_BLOCK_MAX_COUNT', 100);
define('CISION_BLOCK_DEFAULT_CACHE', 60 * 30);
define('CISION_BLOCK_DEFAULT_DATE_FORMAT', 'Y-m-d');
define('CISION_BLOCK_DEFAULT_ITEMS_PER_PAGE', 0);
define('CISION_BLOCK_DEFAULT_IMAGE_STYLE', 1);
define('CISION_BLOCK_CACHE_BIN', 'cache');
define('CISION_BLOCK_CACHE_KEY', 'cision_block');

/**
 * Implements hook_help().
 */
function cision_block_help($path, $arg) {
  if ($path == 'admin/help#cision_block') {
    $filepath = dirname(__FILE__) . '/README.md';
    if (file_exists($filepath)) {
      $readme = file_get_contents($filepath);
    }
    else {
      $filepath = dirname(__FILE__) . '/README.txt';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
    }
    if (!isset($readme)) {
      return NULL;
    }
    if (module_exists('markdown')) {
      $filters = module_invoke('markdown', 'filter_info');
      $info = $filters['filter_markdown'];

      if (function_exists($info['process callback'])) {
        $output = $info['process callback']($readme, NULL);
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }
    }
    else {
      $output = '<pre>' . $readme . '</pre>';
    }

    return $output;
  }
}

/**
 * Implements hook_permission().
 */
function cision_block_permission() {
  return array(
    'administer cision block' => array(
      'title' => t('Cision Block'),
      'description' => t('Configure Cision Block'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function cision_block_theme($existing, $type, $theme, $path) {
  return array(
    'cision_block' => array(
      'variables' => array(
        'items' => NULL,
        'date_format' => NULL,
        'pager' => NULL,
      ),
      'template' => 'cision-block',
      'path' => sprintf('%s/templates', $path),
    ),
    'cision-item' => array(
      'variables' => array(
        'item' => NULL,
        'date_format' => NULL,
      ),
      'template' => 'cision-item',
      'path' => sprintf('%s/templates', $path),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function cision_block_block_info() {
  return array(
    'cision_block' => array(
      'info' => t('Cision Block'),
      'cache' => DRUPAL_NO_CACHE,
    ),
  );
}

/**
 * Validate so a date is in the supplied format.
 *
 * @param string $time
 *   Date string used to create a date in specified format.
 * @param string $format
 *   Format used to create a date.
 *
 * @return bool
 *   Returns true if the supplied date is valid and in
 *   the specified format.
 */
function _cision_block_valid_date($time, $format = 'Y-m-d') {
  $date = date_create_from_format($format, $time);
  return ($date && date_format($date, $format) == $time);
}

/**
 * Validates settings form.
 *
 * @param array $form
 *   Form API style form array.
 * @param array $form_state
 *   Form API style form_state array.
 */
function cision_block_validate(array &$form, array $form_state) {
  // Validate feed source.
  $source = $form_state['values']['cision_block_source'];
  if ($source) {
    $components = parse_url($source);
    if (isset($components['scheme']) && !in_array($components['scheme'], array('http', 'https'))) {
      form_set_error('cision_block_source', t('Only http or https protocols are allowed.'));
    }
    elseif (!valid_url($source, TRUE)) {
      form_set_error('cision_block_source', t('The feed source needs to be a valid URL.'));
    }
  }

  // Validate the cache expire time.
  $expire = (int) $form_state['values']['cision_block_cache_expire'];
  if ($expire < 0) {
    form_set_error('cision_block_cache_expire', t('The cache expire time must be equal or greater than 0.'));
  }

  // Validate feed count.
  $count = (int) $form_state['values']['cision_block_count'];
  if ($count < CISION_BLOCK_MIN_COUNT || $count > CISION_BLOCK_MAX_COUNT) {
    form_set_error('cision_block_count', t('The number of feed items needs to be between @min and @max.', array(
      '@min' => CISION_BLOCK_MIN_COUNT,
      '@max' => CISION_BLOCK_MAX_COUNT,
    )));
  }

  // Validate start and end date.
  $start_date = $form_state['values']['cision_block_start_date'];
  if ($start_date) {
    if (!_cision_block_valid_date($start_date, 'Y-m-d')) {
      form_set_error('cision_block_start_date', t('The start date must be in Y-m-d format.'));
    }
    /*
     * Check so start_date is not in the future.
    elseif (date_create_from_format('Y-m-d', $start_date) >
    date_create_from_format('Y-m-d', date('Y-m-d'))) {
    }*/
  }
  $end_date = $form_state['values']['cision_block_end_date'];
  if ($end_date) {
    if (!_cision_block_valid_date($end_date, 'Y-m-d')) {
      form_set_error('cision_block_end_date', t('The end date must be in Y-m-d format.'));
    }
  }
  if ($start_date && _cision_block_valid_date($start_date) &&
      $end_date && _cision_block_valid_date($end_date)) {
    if (strtotime($start_date) >= strtotime($end_date)) {
      form_set_error('cision_block_start_date', t('The start date must be greater than the end date.'));
      form_set_error('cision_block_end_date', t('The end date cannot be greater than the start date.'));
    }
  }
  // Lowercase any language code.
  $form_state['values']['cision_block_language'] = drupal_strtolower($form_state['values']['cision_block_language']);
  // Validate number of items per page.
  $items_per_page = (int) $form_state['values']['cision_block_items_per_page'];
  if ($items_per_page < 0) {
    form_set_error('cision_block_items_per_page', t('The number of items per page must be 0 or above.'));
  }
}

/**
 * Get all available image styles.
 *
 * @return array
 *   Returns an array of all available image styles.
 */
function _cision_block_image_styles() {
  return array(
    '' => array(
      'label' => t('None'),
    ),
    'DownloadUrl' => array(
      'label' => t('Original Image'),
      'class' => 'image-original',
    ),
    'UrlTo100x100ArResized' => array(
      'label' => t('100x100 Resized'),
      'class' => 'image-100x100-resized',
    ),
    'UrlTo200x200ArResized' => array(
      'label' => t('200x200 Resized'),
      'class' => 'image-200x200-resized',
    ),
    'UrlTo400x400ArResized' => array(
      'label' => t('400x400 Resized'),
      'class' => 'image-400x400-resized',
    ),
    'UrlTo800x800ArResized' => array(
      'label' => t('800x800 Resized'),
      'class' => 'image-800x800-resized',
    ),
    'UrlTo100x100Thumbnail' => array(
      'label' => t('100x100 Thumbnail'),
      'class' => 'image-100x100-thumbnail',
    ),
    'UrlTo200x200Thumbnail' => array(
      'label' => t('200x200 Thumbnail'),
      'class' => 'image-200x200-thumbnail',
    ),
  );
}

/**
 * Get all feed types.
 *
 * @return array
 *   Returns an array of all available feed types.
 */
function _cision_block_feed_types() {
  return array(
    'KMK' => t('Annual Financial statement'),
    'RDV' => t('Annual Report'),
    'PRM' => t('Company Announcement'),
    'RPT' => t('Interim Report'),
    'INB' => t('Invitation'),
    'NBR' => t('Newsletter'),
  );
}

/**
 * Implements hook_form_block_admin_configure_alter().
 */
function cision_block_form_block_admin_configure_alter(&$form, &$form_state, $form_id) {
  if ($form['module']['#value'] == 'cision_block') {
    $form['#validate'][] = 'cision_block_validate';
  }
}

/**
 * Implements hook_block_configure().
 */
function cision_block_block_configure($delta = '') {
  $form = array();

  // Check so user has permission to access the block configuration.
  if (!user_access('administer cision block')) {
    drupal_set_message(t('You are not allowed to edit the @block block.', array('@block' => $delta)), WATCHDOG_WARNING);
    drupal_goto('admin/structure/block/manage');
    drupal_exit();
  }

  $image_styles = _cision_block_image_styles();
  foreach ($image_styles as $key => $style) {
    $image_styles[$key] = $style['label'];
  }
  $image_styles = array_values($image_styles);

  if ($delta == 'cision_block') {
    $form['cision_block_source'] = array(
      '#type' => 'textfield',
      '#title' => t('Cision Feed source'),
      '#description' => t('A valid URL to your cision feed.'),
      '#default_value' => variable_get('cision_block_source'),
    );
    $form['cision_block_count'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of feed items'),
      '#description' => t('The maximum number of items in the feed.'),
      '#default_value' => variable_get('cision_block_count', CISION_BLOCK_DEFAULT_COUNT),
    );
    $form['cision_block_items_per_page'] = array(
      '#type' => 'textfield',
      '#title' => t('Items per page'),
      '#description' => t('The number of items to display per page. Enter 0 to disable pagination.'),
      '#default_value' => variable_get('cision_block_items_per_page', CISION_BLOCK_DEFAULT_ITEMS_PER_PAGE),
    );
    $form['cision_block_feed_types'] = array(
      '#type' => 'select',
      '#title' => t('Feed types'),
      '#description' => t('Type of feed items to include.'),
      '#multiple' => TRUE,
      '#options' => _cision_block_feed_types(),
      '#default_value' => variable_get('cision_block_feed_types', array_keys(_cision_block_feed_types())),
    );
    $form['cision_block_tags'] = array(
      '#type' => 'textfield',
      '#title' => t('Tags'),
      '#description' => t('Defines a filter on tags, this will return releases with these tags. One or several 
tags can be provided separated with a comma.'),
      '#default_value' => variable_get('cision_block_tags'),
    );
    $form['cision_block_start_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Start date'),
      '#description' => t('Defines the start date of the date interval the press releases and/or reports are
collected from. The format is 2001-01-01.'),
      '#default_value' => variable_get('cision_block_start_date'),
    );
    $form['cision_block_end_date'] = array(
      '#type' => 'textfield',
      '#title' => t('End date'),
      '#description' => t('Defines the end date of the date interval the press releases and/or reports are
collected from. The format is 2001-01-01.'),
      '#default_value' => variable_get('cision_block_end_date'),
    );
    $form['cision_block_language'] = array(
      '#type' => 'textfield',
      '#title' => t('Language'),
      '#description' => t('The language code for each feed item, for example <em>en</em> for english.
      Notice that this only is needed in case the feed contains multiple language versions.'),
      '#default_value' => variable_get('cision_block_language'),
    );
    $form['cision_block_regulatory'] = array(
      '#type' => 'checkbox',
      '#title' => t('Regulatory'),
      '#description' => t('Only include regulatory press releases.'),
      '#default_value' => variable_get('cision_block_regulatory'),
    );
    $form['cision_block_dateformat'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom date format'),
      '#description' => t('See the PHP manual for date formats:') . ' <a target="_blank" href="http://php.net/manual/en/datetime.formats.php">' . t('Here') . '</a>',
      '#default_value' => variable_get('cision_block_dateformat', CISION_BLOCK_DEFAULT_DATE_FORMAT),
    );
    $form['cision_block_image_style'] = array(
      '#type' => 'select',
      '#title' => t('Image style'),
      '#description' => t('Select the image format to use.<br />You can set this to None to disable images.'),
      '#options' => $image_styles,
      '#default_value' => variable_get('cision_block_image_style', CISION_BLOCK_DEFAULT_IMAGE_STYLE),
    );
    $form['cision_block_cache_expire'] = array(
      '#type' => 'textfield',
      '#title' => t('Cache expire'),
      '#description' => t('The time in seconds until the cache expires.'),
      '#default_value' => variable_get('cision_block_cache_expire', CISION_BLOCK_DEFAULT_CACHE),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function cision_block_block_save($delta = '', $edit = array()) {
  if ($delta == 'cision_block') {
    variable_set('cision_block_source', $edit['cision_block_source']);
    variable_set('cision_block_count', (int) $edit['cision_block_count']);
    variable_set('cision_block_items_per_page', (int) $edit['cision_block_items_per_page']);
    variable_set('cision_block_feed_types', $edit['cision_block_feed_types']);
    variable_set('cision_block_tags', $edit['cision_block_tags']);
    variable_set('cision_block_start_date', $edit['cision_block_start_date']);
    variable_set('cision_block_end_date', $edit['cision_block_end_date']);
    variable_set('cision_block_dateformat', $edit['cision_block_dateformat']);
    variable_set('cision_block_image_style', (int) $edit['cision_block_image_style']);
    variable_set('cision_block_cache_expire', (int) $edit['cision_block_cache_expire']);
    variable_set('cision_block_language', $edit['cision_block_language']);
    variable_set('cision_block_regulatory', (int) $edit['cision_block_regulatory']);

    // Clear any cache entries.
    db_delete('cache')
      ->condition('cid', '%' . db_like('cision') . '%', 'LIKE')
      ->execute();
  }
}

/**
 * Implements hook_block_view().
 */
function cision_block_block_view($delta = '') {
  $block = array();
  if ($delta == 'cision_block') {
    $cache = &drupal_static(__FUNCTION__);
    if (empty($cache)) {
      $query = '_' . implode('_', drupal_get_query_parameters());
      $cache = cache_get(CISION_BLOCK_CACHE_KEY . $query, CISION_BLOCK_CACHE_BIN);
      if ($cache && !empty($cache->data)) {
        if (time() > $cache->expire) {
          cache_clear_all(CISION_BLOCK_CACHE_KEY . $query, CISION_BLOCK_CACHE_BIN);
        }
        $block = $cache->data;
      }
      else {
        $items = _cision_block_get_feeds();
        $pager = '';
        if (count($items)) {
          $pager = _cision_block_pager($items, variable_get('cision_block_items_per_page'));
        }
        $dateformat = variable_get('cision_block_dateformat', CISION_BLOCK_DEFAULT_DATE_FORMAT);
        $block['content'] = array(
          '#theme' => 'cision_block',
          '#items' => $items,
          '#pager' => $pager,
          '#date_format' => $dateformat,
          '#attached' => array(
            'css' => array(
              drupal_get_path('module', 'cision_block') . '/css/cision-block.css',
            ),
          ),
        );
        if (variable_get('cision_block_cache_expire') > 0) {
          cache_set(CISION_BLOCK_CACHE_KEY . $query, $block, CISION_BLOCK_CACHE_BIN, time() + variable_get('cision_block_cache_expire'));
        }
      }
    }
    else {
      $block = $cache->data;
    }
  }
  return $block;
}

/**
 * Creates and returns markup for a pager.
 *
 * @param array $items
 *   Array of all feed items.
 * @param int $items_per_page
 *   Number of items per page.
 *
 * @return string
 *   Markup for the generated pager.
 */
function _cision_block_pager(array &$items, $items_per_page) {
  $output = '';
  if ($items_per_page > 0) {
    $max = ceil(count($items) / $items_per_page);
    $params = drupal_get_query_parameters();
    $active = (isset($params['id']) && $params['id'] == 'cision_block' ? $params['page'] : 0);
    if ($max > 1) {
      $output = '<ul class="cision-pager">';
      for ($i = 0; $i < $max; $i++) {
        $output .= '<li><a href="?id=cision_block&page=' . $i . '"' . ($active == $i ? ' class="active"' : '') . '>' . ($i + 1) . '</a></li>';
      }

      if ($active >= 0 && $active < $max) {
        $items = array_slice($items, $active * $items_per_page, $items_per_page);
      }
      $output .= '</ul>';
    }
  }
  return $output;
}

/**
 * Returns a list of feeds from cision.
 *
 * @return array
 *   All feed items.
 */
function _cision_block_get_feeds() {
  $source = variable_get('cision_block_source');
  $feed = NULL;
  if (!valid_url($source, TRUE)) {
    return NULL;
  }
  $query_args = http_build_query(array(
    'detailLevel' => 'detail',
    'pageSize' => variable_get('cision_block_count', CISION_BLOCK_DEFAULT_COUNT),
    'tags' => variable_get('cision_block_tags'),
    'format' => 'json',
    'startDate' => variable_get('cision_block_start_date'),
    'endDate' => variable_get('cision_block_end_date'),
  ));
  $response = drupal_http_request($source . '?' . $query_args);
  switch ($response->code) {
    case 304:
      break;

    case 301:
    case 200:
    case 302:
    case 307:
      if (isset($response->data)) {
        $feed = json_decode($response->data);
      }
      break;

    default:
      break;
  }

  return ($feed ? _cision_block_get_mapping_source($feed) : NULL);
}

/**
 * Hook that can be used to add more content to each feed item.
 *
 * @param array $item
 *   A single feed item.
 * @param \stdClass $release
 *   A feed item.
 *
 * @return array
 *   A mapped feed item.
 */
function cision_block_map_cision_sources(array $item, \stdClass $release) {
  return $item;
}

/**
 * Maps data from a cision feed item into an array.
 *
 * @param \stdClass $release
 *   A raw feed item.
 * @param string $image_style
 *   The image style to use.
 *
 * @return array
 *   A mapped release item.
 */
function _cision_block_map_item(\stdClass $release, $image_style) {
  $item = array();
  $item['Id'] = (int) $release->Id;
  $item['Title'] = check_plain($release->Title);
  $item['PublishDate'] = strtotime($release->PublishDate);
  $item['Intro'] = check_plain($release->Intro);
  $item['Body'] = check_plain($release->Body);
  if ($image_style) {
    foreach ($release->Images as &$image) {
      $image->DownloadUrl = check_url($image->{$image_style});
      $image->Description = check_plain($image->Description);
      $image->Title = check_plain($image->Title);
    }
  }
  else {
    $release->Images = array();
  }
  $item['Images'] = $release->Images;
  $item['CisionWireUrl'] = check_url($release->CisionWireUrl);
  return $item;
}

/**
 * Mapping function.
 *
 * @param \stdClass $feed
 *   Object containing all feed items.
 *
 * @return array
 *   An array of mapped items.
 */
function _cision_block_get_mapping_source(\stdClass $feed) {
  $items = NULL;
  $image_styles = array_keys(_cision_block_image_styles());
  $image_style = variable_get('cision_block_image_style');
  $image_style = $image_styles[$image_style];
  $feed_types = variable_get('cision_block_feed_types');
  $language = variable_get('cision_block_language');
  $regulatory = variable_get('cision_block_regulatory');

  if (isset($feed->Releases)) {
    foreach ($feed->Releases as $release) {
      if (!is_object($release)) {
        continue;
      }
      // Check so regulatory if option selected.
      if ($regulatory && (int) $release->IsRegulatory == 0) {
        continue;
      }
      // Check so valid feed type.
      if (!in_array($release->InformationType, $feed_types)) {
        continue;
      }
      // Check so article is in correct language.
      if (!empty($language) && $release->LanguageCode != $language) {
        continue;
      }

      $item = _cision_block_map_item($release, $image_style);

      // Let other modules alter the content.
      foreach (module_implements('map_cision_sources') as $module) {
        $function = $module . '_map_cision_sources';
        $item = $function($item, $release);
      }
      $items[] = (object) $item;
    }
  }
  return $items;
}
