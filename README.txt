INTRODUCTION
============

This modules creates a custom block which can
be configured to show a specific number of feed items
from the supplied cision source.

REQUIREMENTS
============

- Drupal 7.x
  http://drupal.org/project/drupal

INSTALLATION
============

- Install and enable the cision_block module.
- At the block configuration page place a 'Cision Block' in a region.
- Add the 'administer cision block' permission to the desired role.
- In the configuration form for the block, set the number of 
feed items to display and add the source for the feed.

CONFIGURATION
=============

- Enable the Cision Block under admin/structure/block.
- Configure the block.

MAINTAINERS
-----------

Current maintainers:
 * Krister Andersson (Cyclonecode) - https://www.drupal.org/user/1225156
