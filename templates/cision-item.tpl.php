<?php

/**
 * @file
 * Default theme implementation to display a single cision feed item.
 *
 * @todo:
 * Use semantic <header></header>
 *
 * Available variables:
 * - item: A feed item.
 * - date_format: The desired date format.
 */
?>
<article class="cision-feed-item">
  <h2><?php print $item->Title; ?></h2>
  <time><?php print format_date($item->PublishDate, 'custom', $date_format); ?></time>
  <p>
  <?php if (isset($item->Images[0])): ?>
  <span class="cision-item-media">
  <img src="<?php print $item->Images[0]->DownloadUrl; ?>" alt="<?php print $item->Images[0]->Description; ?>" />
  </span>
  <?php endif; ?>
  <?php print text_summary($item->Intro ? $item->Intro : $item->Body); ?>
  </p>
  <?php print l(t('Read more'), $item->CisionWireUrl, array('attributes' => array('target' => '_blank'))); ?>
</article>
