<?php

/**
 * @file
 * Default theme implementation to display a cision block.
 *
 * Available variables:
 * - items: An array of feed items collected from cision.
 * - pager: A rendered pager.
 * - date_format: The desired date format.
 */
?>
<section class="cision-feed-wrapper">
<?php if (count($items)): ?>
<?php foreach ($items as $item): ?>
  <?php print theme('cision-item', array(
                'item' => $item,
                'date_format' => $date_format,
            )
        );
   ?>
<?php endforeach; ?>
<?php endif; ?>
<?php print $pager; ?>
</section>
